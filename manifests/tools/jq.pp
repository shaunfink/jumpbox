# jumpbox::tools::jq
#
# This class downloads jq to the target system
#
# @note    : If github_release_tag is set to latest, it will download the latest
#            version of the tool, otherwsie you need to specify the specific
#            release tag to download.
#
# @example : $github_release_tag = 'latest'
#          : $github_release_tag = 'jq-1.3'

class jumpbox::tools::jq (
  $github_release_tag = 'latest',
  $jq_destination = '/usr/local/bin/jq'
){
  require ::wget

  if $github_release_tag == 'latest' {
    $jq_source = $facts['jq_download_url']
  } else {
    $jq_source = "https://github.com/stedolan/jq/releases/download/${github_release_tag}/jq-linux-x86_64"
  }

  wget::fetch { "$jq_source" :
    destination => "$jq_destination",
    timeout     => 15,
    verbose     => true,
  }

  file { "$jq_destination" :
    ensure => 'file',
    mode   => 'a+x',
    owner  => 'root',
    group  => 'root',
  }
}
