# jumpbox::tools::om
#
# This class downloads om to the target system
#
# @note    : If github_release_tag is set to latest, it will download the latest
#            version of the tool, otherwsie you need to specify the specific
#            release tag to download.
#
# @example : $github_release_tag = 'latest'
#          : $github_release_tag = '0.31.0'

class jumpbox::tools::om (
  $github_release_tag = 'latest',
  $om_destination = '/usr/local/bin/om'
){
  require ::wget

  if $github_release_tag == 'latest' {
    $om_source = $facts['om_download_url']
  } else {
      $om_source = "https://github.com/pivotal-cf/om/releases/download/${github_release_tag}/om-linux"
  }

  wget::fetch { "$om_source" :
    destination => "$om_destination",
    timeout     => 15,
    verbose     => true,
  }

  file { "$om_destination" :
    ensure => 'file',
    mode   => 'a+x',
    owner  => 'root',
    group  => 'root',
  }
}
