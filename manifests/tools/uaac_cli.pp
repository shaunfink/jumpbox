# jumpbox::tools::uaac_cli
#
# Install the uaac-cli ruby gem.
#
# @note    : You can specify the version of the gem to install using uaac_cli_version
#
# @example : $uaac_cli_version = 'latest'
# @example : $uaac_cli_version = 'latest'

class jumpbox::tools::uaac_cli (
  $uaac_cli_version = 'latest',
){
  package { 'cf-uaac':
    ensure   => "$uaac_cli_version",
    provider => 'gem',
  }
}
