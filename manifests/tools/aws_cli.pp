# jumpbox::tools::aws_cli
#
# This class downloads and installs the aws cli to the target system
#
# @note    : If aws_cli_version is set to latest, it will download the latest
#            version of the tool, otherwsie you need to specify the specific
#            release tag to download.
#
# @example : $aws_cli_version = 'latest'
#          : $aws_cli_version = '1.10.22'

class jumpbox::tools::aws_cli (
    $aws_cli_version = 'latest',
){
  python::pip { 'awscli' :
    pkgname => 'awscli',
    ensure  => "$aws_cli_version",
    timeout => 1800,
   }
}
