# jumpbox::tools::bosh_cli
#
# This class downloads jq to the target system
#
# @note    : If bosh_cli_version is set to latest, it will download the latest
#            version of the tool, otherwsie you need to specify the specific
#            release tag to download.
#
# @example : $bosh_cli_version = 'latest'
#          : $bosh_cli_version = '2.0.7'

class jumpbox::tools::bosh_cli (
  $bosh_cli_version = '2.0.7',
  $bosh_cli_destination = '/usr/local/bin/bosh',
  $download_home = "$jumpbox::server::working_dir/binaries",
){
  require jumpbox::tools::aws_cli
  require ::wget

  if $bosh_cli_version == 'latest' {
    $version = $facts['latest_bosh_cli_version']
  } else {
    $version = $bosh_cli_version
  }

  $bosh_cli_filename = "bosh-cli-$version-linux-amd64"

  wget::fetch { "https://s3.amazonaws.com/bosh-cli-artifacts/$bosh_cli_filename" :
    destination => "$bosh_cli_destination",
    timeout     => 15,
    verbose     => true,
  }

  file { "$bosh_cli_destination" :
    ensure => 'file',
    mode   => 'a+x',
    owner  => 'root',
    group  => 'root',
  }
}
