# jumpbox::tools::yaml_patch
#
# This class downloads yaml-patch to the target system
#
# @note    : If github_release_tag is set to latest, it will download the latest
#            version of the tool, otherwsie you need to specify the specific
#            release tag to download.
#
# @example : $github_release_tag = 'latest'
#          : $github_release_tag = 'v0.0.6'

class jumpbox::tools::yaml_patch (
  $github_release_tag = 'latest',
  $yaml_patch_destination = '/usr/local/bin/yaml-patch'
){
  require ::wget

  if $github_release_tag == 'latest' {
    $yaml_patch_source = $facts['yaml_patch_download_url']
  } else {
      $yaml_patch_source = "https://github.com/krishicks/yaml-patch/releases/download/${github_release_tag}/yaml_patch_linux"
  }

  wget::fetch { "$yaml_patch_source" :
    destination => "$yaml_patch_destination",
    timeout     => 15,
    verbose     => true,
  }

  file { "$yaml_patch_destination" :
    ensure => 'file',
    mode   => 'a+x',
    owner  => 'root',
    group  => 'root',
  }
}
