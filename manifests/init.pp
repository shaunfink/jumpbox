# jumpbox
#
# The initial class for the jumpbox module

class jumpbox {
  # Include dependant modules
  include ::epel
  include ::wget
  include ::ruby
  include ::stdlib
  include ::yum

  # Include jumpbox specific modules
  include jumpbox::packages
  include jumpbox::server

  # Include specific tools
  include jumpbox::tools::jq
  include jumpbox::tools::om
  include jumpbox::tools::yaml_patch
  include jumpbox::tools::aws_cli
  include jumpbox::tools::bosh_cli
  include jumpbox::tools::uaac_cli
}
