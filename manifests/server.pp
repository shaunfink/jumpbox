# jumpbox::server
#
# This class configures the server itself with the following:
# - Team admin group
# - Team admin user
# - Working directories
#
# @note    : You can specify your own team values for the user, group and directory configs.
#
# @example : $team_admin = 'cloudadmin',
#          : $team_admin_comment = 'Cloud Team Admin',
#          : $team_admin_id = '501',
#          : $team_admin_shell = '/bin/bash',
#          : $team_group = 'cloud',
#          : $team_group_id = '502',
#          : $working_dir = '/cloud',
#          : $home_dir = '/home',

class jumpbox::server (
    $team_admin = 'cloudadmin',
    $team_admin_comment = 'Cloud Team Admin',
    $team_admin_id = '501',
    $team_admin_shell = '/bin/bash',
    $team_group = 'cloud',
    $team_group_id = '502',
    $working_dir = '/cloud',
    $home_dir = '/home',
){
  # Set up the team admin group
  group { "$team_group" :
    ensure => 'present',
    gid    => "$team_group_id",
  }

  # Create the team admin user
  user { "$team_admin" :
    ensure  => 'present',
    gid     => "$team_group_id",
    home    => "$home_dir/$team_admin",
    comment => "$team_admin_comment",
    groups  => "$team_group",
    shell   => "$team_admin_shell",
    uid     => "$team_admin_id",
  }

  # Create cloud directories for our stuff
  file { "$working_dir" :
    ensure => 'directory',
    owner  => "$team_admin",
    group  => "$team_group",
    mode   => '0775',
  }
  file { "$working_dir/workspace" :
    ensure => 'directory',
    owner  => "$team_admin",
    group  => "$team_group",
    mode   => '0775',
  }
  file { "$working_dir/binaries" :
    ensure => 'directory',
    owner  => "$team_admin",
    group  => "$team_group",
    mode   => '0775',
  }
}
