# jumpbox::packages
#
# This class downloads packages from unix repo's.
#
# @note     : Versions need to be specified for Python and PIP
#
# @versions : $pythonVersion = 'python34',
#           : $pipVersion = '9.0.1'

class jumpbox::packages (
    $pythonVersion = 'system',
    $pipVersion = '9.0.1'
){
  require ::epel
  require ::jumpbox::server

  # Essential tools
  package { 'curl' :
    ensure => 'installed',
  }
  package { 'git' :
    ensure => 'installed',
  }
  package { 's3cmd' :
    ensure => 'installed',
  }

  # Network tools
  package { 'nmap' :
    ensure => 'installed',
  }
  package { 'tcpdump' :
    ensure => 'installed',
  }
  package { 'traceroute' :
    ensure => 'installed',
  }
  package { 'iperf' :
    ensure => 'installed',
  }

  # Development tools
  yum::group { 'Development Tools' :
    ensure => present,
  }

  # Python and PIP
  class { 'python' :
    ensure     => 'latest',
    version    => "$pythonVersion",
    pip        => 'latest',
    dev        => 'absent',
    virtualenv => 'absent',
    gunicorn   => 'absent',
  }
  python::pip { 'pip' :
    ensure     => "$pipVersion",
    pkgname    => 'pip',
  }
}
