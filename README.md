
# jumpbox


#### Table of Contents

1. [Description](#description)
    * [Tools installed by jumpbox](#tools-installed-by-jumpbox)
2. [Setup - The basics of getting started with jumpbox](#setup)
    * [What jumpbox affects](#what-jumpbox-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with jumpbox](#beginning-with-jumpbox)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module sets up a jumpbox with a range of useful tools for managing a CloudFoundry based environment. The module is still in development and more information will be provided once a release is ready.

*You can give more descriptive information in a second paragraph. This paragraph should answer the questions: "What does this module do?" and "Why would I use it?" If your module has a range of functionality (installation, configuration, management, etc.), this is the time to mention it.*

### Tools installed by this module
A range of tools are installed on the jumpbox. Below is a list of these tools.

#### File management tools
- [curl](https://curl.haxx.se) - A utility for getting files from remote servers (FTP, HTTP, and others).
- [wget](https://www.gnu.org/software/wget) - A utility for retrieving files using the HTTP or FTP protocols.

#### Version control tools
- [git](https://git-scm.com) - Opensource version control system.

#### Network diagnostic tools
- [nmap](https://nmap.org) - Network exploration tool and security scanner.
- [traceroute](https://en.wikipedia.org/wiki/Traceroute) - Traces the route taken by packets over an IPv4/IPv6 network.
- [tcpdump](https://www.tcpdump.org) - A network traffic monitoring tool.
- [iperf](https://iperf.fr) - Measurement tool for TCP/UDP bandwidth performance.

#### Pivotal-specific tools
- [om](https://github.com/pivotal-cf/om) - Small sharp tool for deploying products to ops-manager.

#### CloudFoundry-specific tools
- [bosh](https://github.com/cloudfoundry/bosh-cli) - BOSH 2.0 Golang CLI.

#### Cloud-specific tools
- [aws-cli](https://aws.amazon.com/cli) - A unified tool to manage your AWS services.

#### Development tools
- [python](https://www.python.org) - Python programming language.

#### Other Useful tools
- [jq](http://stedolan.github.io/jq) - Command-line JSON processor.
- [yaml-patch](https://github.com/krishicks/yaml-patch) - A library to apply YAML versions of RFC6902 patches.
- [pip](https://pypi.python.org/pypi/pip) - The PyPA recommended tool for installing Python packages.


## Setup

### What jumpbox affects **OPTIONAL**

This module is still under development, and this section will be updated once it's ready for release.

*If it's obvious what your module touches, you can skip this section. For example, folks can probably figure out that your mysql_instance module affects their MySQL instances.

If there's more that they should know about, though, this is the place to mention:

- Files, packages, services, or operations that the module will alter, impact, or execute.
- Dependencies that your module automatically installs.
- Warnings or other important notices.*

### Setup Requirements

This module requires a few additional modules to function correctly.
- [puppetlabs/stdlib](https://forge.puppet.com/puppetlabs/stdlib) - Standard library of resources for Puppet modules.
- [stahnma/epel](https://forge.puppet.com/stahnma/epel) - Setup the EPEL package repo.
- [maestrodev/wget](https://forge.puppet.com/maestrodev/wget) - Download files with wget.
- [puppetlabs/ruby](https://forge.puppet.com/puppetlabs/ruby) - Manages Ruby and Rubygems.
- [stankevich/python](https://forge.puppet.com/stankevich/python) - Python Module

### Beginning with jumpbox

This section will be updated once it's ready for release.

*The very basic steps needed for a user to get the module up and running. This can include setup steps, if necessary, or it can be an example of the most basic use of the module.*

## Usage

This module is still under development, and this section will be updated once it's ready for release.

## Reference

This module is still under development, and this section will be updated once it's ready for release.

*Users need a complete list of your module's classes, types, defined types providers, facts, and functions, along with the parameters for each. You can provide this list either via Puppet Strings code comments or as a complete list in the README Reference section.

- If you are using Puppet Strings code comments, this Reference section should include Strings information so that your users know how to access your documentation.

- If you are not using Puppet Strings, include a list of all of your classes, defined types, and so on, along with their parameters. Each element in this listing should include:

  - The data type, if applicable.
  - A description of what the element does.
  - Valid values, if the data type doesn't make it obvious.
  - Default value, if any.*

## Limitations

Currently development on this module is on CentOS EL7. There will be some testing done on Ubuntu eventually. No other OS's are planned.

## Development

This module is still under development, and this section will be updated once it's ready for release.

## Release Notes

This module is still under development, and this section will be updated once it's ready for release.
