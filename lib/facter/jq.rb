require 'facter'
Facter.add(:jq_download_url) do
    setcode do
        Facter::Core::Execution.execute("curl -s --tlsv1.2 https://api.github.com/repos/stedolan/jq/releases/latest | grep browser_download_url | grep linux | grep 64 | grep -v zip | cut -d '\"' -f 4")
    end
end
