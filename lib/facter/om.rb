require 'facter'
Facter.add(:om_download_url) do
    setcode do
        Facter::Core::Execution.execute("curl -s --tlsv1.2 https://api.github.com/repos/pivotal-cf/om/releases/latest | grep browser_download_url | grep linux | grep -v zip | cut -d '\"' -f 4")
    end
end
