require 'facter'
Facter.add(:latest_bosh_cli_version) do
    setcode do
        Facter::Core::Execution.execute("aws s3 ls s3://bosh-cli-artifacts --no-sign-request | grep linux | sort | tail -n 1 | awk '{print $4}' | cut -d - -f 3")
    end
end
